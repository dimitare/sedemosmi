package com.angdim.sedemosmi.tv

import com.angdim.sedemosmi.tv.data.services.remote.SedemOsmiServiceApi
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.mock.BehaviorDelegate
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior
import javax.inject.Singleton

@Module
class RetrofitModule {
    @Singleton
    @Provides
    fun providesRetrofit(okHttpClient: OkHttpClient): Retrofit {
        val map = jacksonObjectMapper()
        map.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        return Retrofit.Builder()
            .addConverterFactory(JacksonConverterFactory.create(jacksonObjectMapper()))
            .baseUrl(BuildConfig.SEDEM_OSMI_SERVICE_URL)
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun providesBehaviorDelegate(): BehaviorDelegate<SedemOsmiServiceApi> {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://mock-service-api")
            .addConverterFactory(JacksonConverterFactory.create(jacksonObjectMapper()))
            .build()
        val mockRetrofit = MockRetrofit.Builder(retrofit)
            .networkBehavior(NetworkBehavior.create())
            .build()

        return mockRetrofit.create(SedemOsmiServiceApi::class.java)
    }

    @Provides
    fun providesOkHttp(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .build()
    }

    @Provides
    fun providesHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }
}