package com.angdim.sedemosmi.tv

import com.angdim.sedemosmi.tv.data.services.SedemOsmiDataSource
import com.angdim.sedemosmi.tv.data.services.SedemOsmiRepository
import com.angdim.sedemosmi.tv.data.services.SedemOsmiRepositoryImpl
import com.angdim.sedemosmi.tv.data.services.remote.SedemOsmiRemoteDataSource
import com.angdim.sedemosmi.tv.data.services.remote.SedemOsmiServiceApi
import com.angdim.sedemosmi.tv.data.services.remote.SedemOsmiServiceMock
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.mock.BehaviorDelegate
import javax.inject.Named
import javax.inject.Singleton

@Module
class SedemOsmiRemoteRepositoryModule {
    @Provides
    @Singleton
    fun providesService(
        sedemOsmiSource: SedemOsmiDataSource
    ): SedemOsmiRepository {
        return SedemOsmiRepositoryImpl(
            sedemOsmiSource
        )
    }

    @Provides
    fun provideDemoDataSource(@Named("serviceApi") sedemOsmiService: SedemOsmiServiceApi): SedemOsmiDataSource {
        return SedemOsmiRemoteDataSource(sedemOsmiService)
    }

    @Provides
    @Named("serviceApi")
    fun providesDemoService(retrofit: Retrofit): SedemOsmiServiceApi {
        return retrofit.create(SedemOsmiServiceApi::class.java)
    }

    @Provides
    @Named("mockServiceApi")
    fun provideMockDemoService(delegate: BehaviorDelegate<SedemOsmiServiceApi>): SedemOsmiServiceApi {
        return SedemOsmiServiceMock(delegate)
    }
}