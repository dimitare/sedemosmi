package com.angdim.sedemosmi.tv.data.services.remote

import com.angdim.sedemosmi.tv.BuildConfig
import com.angdim.sedemosmi.tv.data.model.HomepageResponse
import com.angdim.sedemosmi.tv.data.model.VideoPageResponse
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import retrofit2.Call
import retrofit2.mock.BehaviorDelegate

class SedemOsmiServiceMock internal constructor(val delegate: BehaviorDelegate<SedemOsmiServiceApi>) :
    SedemOsmiServiceApi {
    val data = "{\n" +
            "    \"code\": 0,\n" +
            "    \"sections\": [\n" +
            "        {\n" +
            "            \"id\": \"carousel\",\n" +
            "            \"title\": \"Carousel\",\n" +
            "            \"pageViews\": [\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/05/20200518_PROMO_AKTIORI.jpg\",\n" +
            "                    \"videoTitle\": \"Гледайте „Вечерта на актьорите“ в понеделник, 18 май\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/gledaite-aktiorite-18-05-2020/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/gledaite-aktiorite-18-05-2020/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/05/20200511_PROMO_STUDIO-1.jpg\",\n" +
            "                    \"videoTitle\": \"Гледайте „Студио Хъ“ в понеделник\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/gledaite-studio-x-ponedelnik-18-05-2020/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/gledaite-studio-x-ponedelnik-18-05-2020/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2019/11/VECHERNOTO_SHOW_NA_SLAVI_TRIFONOV.jpg\",\n" +
            "                    \"videoTitle\": \"Лого Вечерното шоу на Слави Трифонов\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/gledaite-vechernoto-show-5-2020/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/gledaite-vechernoto-show-5-2020/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2019/11/SHOUTO_NA_SCENARISTITE_23_00.jpg\",\n" +
            "                    \"videoTitle\": \"Лого Шоуто на сценаристите\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/gledaite-shouto-na-scenaristite-05-2020-g/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/gledaite-shouto-na-scenaristite-05-2020-g/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/02/20200316_PROMO_MATE.jpg\",\n" +
            "                    \"videoTitle\": \"Гледайте Mate Kitchen по 7/8 TV!\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/gledaite-mate-kitchen-7-8-tv/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/gledaite-mate-kitchen-7-8-tv/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/05/20200513_PROMO_IMG.jpg\",\n" +
            "                    \"videoTitle\": \"7/8 TV – думите остават! Проф. Иво Христов\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/7-8-tv-%d0%b4%d1%83%d0%bc%d0%b8%d1%82%d0%b5-%d0%be%d1%81%d1%82%d0%b0%d0%b2%d0%b0%d1%82-2/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/7-8-tv-%d0%b4%d1%83%d0%bc%d0%b8%d1%82%d0%b5-%d0%be%d1%81%d1%82%d0%b0%d0%b2%d0%b0%d1%82-2/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/05/20200504_PROMO_3.jpg\",\n" +
            "                    \"videoTitle\": \"7/8 TV – силата е в истината! Люба Ризова\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/7-8-tv-%d1%81%d0%b8%d0%bb%d0%b0%d1%82%d0%b0-%d0%b5-%d0%b2-%d0%b8%d1%81%d1%82%d0%b8%d0%bd%d0%b0%d1%82%d0%b0-4/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/7-8-tv-%d1%81%d0%b8%d0%bb%d0%b0%d1%82%d0%b0-%d0%b5-%d0%b2-%d0%b8%d1%81%d1%82%d0%b8%d0%bd%d0%b0%d1%82%d0%b0-4/\"\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": \"нови епизоди\",\n" +
            "            \"title\": \"Нови епизоди\",\n" +
            "            \"pageViews\": [\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/05/3_1-13.jpg\",\n" +
            "                    \"videoTitle\": \"Шоуто на сценаристите, 14 май 2020 г.\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/production/%d1%88%d0%be%d1%83%d1%82%d0%be-%d0%bd%d0%b0-%d1%81%d1%86%d0%b5%d0%bd%d0%b0%d1%80%d0%b8%d1%81%d1%82%d0%b8%d1%82%d0%b5-14-%d0%bc%d0%b0%d0%b9-2020-%d0%b3/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/production/%d1%88%d0%be%d1%83%d1%82%d0%be-%d0%bd%d0%b0-%d1%81%d1%86%d0%b5%d0%bd%d0%b0%d1%80%d0%b8%d1%81%d1%82%d0%b8%d1%82%d0%b5-14-%d0%bc%d0%b0%d0%b9-2020-%d0%b3/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/05/9_1-1.jpg\",\n" +
            "                    \"videoTitle\": \"Вечерното шоу на Слави Трифонов, 14 май 2020 г.\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/production/%d0%b2%d0%b5%d1%87%d0%b5%d1%80%d0%bd%d0%be%d1%82%d0%be-%d1%88%d0%be%d1%83-%d0%bd%d0%b0-%d1%81%d0%bb%d0%b0%d0%b2%d0%b8-%d1%82%d1%80%d0%b8%d1%84%d0%be%d0%bd%d0%be%d0%b2-14-%d0%bc%d0%b0%d0%b9-2020/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/production/%d0%b2%d0%b5%d1%87%d0%b5%d1%80%d0%bd%d0%be%d1%82%d0%be-%d1%88%d0%be%d1%83-%d0%bd%d0%b0-%d1%81%d0%bb%d0%b0%d0%b2%d0%b8-%d1%82%d1%80%d0%b8%d1%84%d0%be%d0%bd%d0%be%d0%b2-14-%d0%bc%d0%b0%d0%b9-2020/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/05/1_1-12.jpg\",\n" +
            "                    \"videoTitle\": \"Вечерта на… Криско, 14 май 2020 г.\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/production/%d0%b2%d0%b5%d1%87%d0%b5%d1%80%d1%82%d0%b0-%d0%bd%d0%b0-%d0%ba%d1%80%d0%b8%d1%81%d0%ba%d0%be-14-%d0%bc%d0%b0%d0%b9-2020-%d0%b3/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/production/%d0%b2%d0%b5%d1%87%d0%b5%d1%80%d1%82%d0%b0-%d0%bd%d0%b0-%d0%ba%d1%80%d0%b8%d1%81%d0%ba%d0%be-14-%d0%bc%d0%b0%d0%b9-2020-%d0%b3/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/05/1_1-11.jpg\",\n" +
            "                    \"videoTitle\": \"Студио Хъ – доц. д-р Момчил Дойчев, 14 май 2020 г.\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/production/%d1%81%d1%82%d1%83%d0%b4%d0%b8%d0%be-%d1%85%d1%8a-%d0%b4%d0%be%d1%86-%d0%b4-%d1%80-%d0%bc%d0%be%d0%bc%d1%87%d0%b8%d0%bb-%d0%b4%d0%be%d0%b9%d1%87%d0%b5%d0%b2-14-%d0%bc%d0%b0%d0%b9-2020-%d0%b3/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/production/%d1%81%d1%82%d1%83%d0%b4%d0%b8%d0%be-%d1%85%d1%8a-%d0%b4%d0%be%d1%86-%d0%b4-%d1%80-%d0%bc%d0%be%d0%bc%d1%87%d0%b8%d0%bb-%d0%b4%d0%be%d0%b9%d1%87%d0%b5%d0%b2-14-%d0%bc%d0%b0%d0%b9-2020-%d0%b3/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/05/3_1-12.jpg\",\n" +
            "                    \"videoTitle\": \"Студио Хъ – Владимир Береану, 14 май 2020 г.\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/production/%d1%81%d1%82%d1%83%d0%b4%d0%b8%d0%be-%d1%85%d1%8a-%d0%b2%d0%bb%d0%b0%d0%b4%d0%b8%d0%bc%d0%b8%d1%80-%d0%b1%d0%b5%d1%80%d0%b5%d0%b0%d0%bd%d1%83-14-%d0%bc%d0%b0%d0%b9-2020-%d0%b3/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/production/%d1%81%d1%82%d1%83%d0%b4%d0%b8%d0%be-%d1%85%d1%8a-%d0%b2%d0%bb%d0%b0%d0%b4%d0%b8%d0%bc%d0%b8%d1%80-%d0%b1%d0%b5%d1%80%d0%b5%d0%b0%d0%bd%d1%83-14-%d0%bc%d0%b0%d0%b9-2020-%d0%b3/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/05/5_1-8.jpg\",\n" +
            "                    \"videoTitle\": \"Студио Хъ – Светослав Терзиев, 14 май 2020 г.\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/production/%d1%81%d1%82%d1%83%d0%b4%d0%b8%d0%be-%d1%85%d1%8a-%d1%81%d0%b2%d0%b5%d1%82%d0%be%d1%81%d0%bb%d0%b0%d0%b2-%d1%82%d0%b5%d1%80%d0%b7%d0%b8%d0%b5%d0%b2-14-%d0%bc%d0%b0%d0%b9-2020-%d0%b3/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/production/%d1%81%d1%82%d1%83%d0%b4%d0%b8%d0%be-%d1%85%d1%8a-%d1%81%d0%b2%d0%b5%d1%82%d0%be%d1%81%d0%bb%d0%b0%d0%b2-%d1%82%d0%b5%d1%80%d0%b7%d0%b8%d0%b5%d0%b2-14-%d0%bc%d0%b0%d0%b9-2020-%d0%b3/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/05/7_1-1.jpg\",\n" +
            "                    \"videoTitle\": \"Студио Хъ – Любомира Ганчева, 14 май 2020 г.\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/production/%d1%81%d1%82%d1%83%d0%b4%d0%b8%d0%be-%d1%85%d1%8a-%d0%bb%d1%8e%d0%b1%d0%be%d0%bc%d0%b8%d1%80%d0%b0-%d0%b3%d0%b0%d0%bd%d1%87%d0%b5%d0%b2%d0%b0-14-%d0%bc%d0%b0%d0%b9-2020-%d0%b3/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/production/%d1%81%d1%82%d1%83%d0%b4%d0%b8%d0%be-%d1%85%d1%8a-%d0%bb%d1%8e%d0%b1%d0%be%d0%bc%d0%b8%d1%80%d0%b0-%d0%b3%d0%b0%d0%bd%d1%87%d0%b5%d0%b2%d0%b0-14-%d0%bc%d0%b0%d0%b9-2020-%d0%b3/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/05/11.jpg\",\n" +
            "                    \"videoTitle\": \"Студио Хъ – коментар на Слави Трифонов, 14 май 2020 г.\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/production/%d1%81%d1%82%d1%83%d0%b4%d0%b8%d0%be-%d1%85%d1%8a-%d0%ba%d0%be%d0%bc%d0%b5%d0%bd%d1%82%d0%b0%d1%80-%d0%bd%d0%b0-%d1%81%d0%bb%d0%b0%d0%b2%d0%b8-%d1%82%d1%80%d0%b8%d1%84%d0%be%d0%bd%d0%be%d0%b2-14/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/production/%d1%81%d1%82%d1%83%d0%b4%d0%b8%d0%be-%d1%85%d1%8a-%d0%ba%d0%be%d0%bc%d0%b5%d0%bd%d1%82%d0%b0%d1%80-%d0%bd%d0%b0-%d1%81%d0%bb%d0%b0%d0%b2%d0%b8-%d1%82%d1%80%d0%b8%d1%84%d0%be%d0%bd%d0%be%d0%b2-14/\"\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": \"музика\",\n" +
            "            \"title\": \"Музика\",\n" +
            "            \"pageViews\": [\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/04/20200407_KLIP_1920.jpg\",\n" +
            "                    \"videoTitle\": \"Слави, ТоТо Н и Ку-ку бенд – „Заедно“\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/%d1%81%d0%bb%d0%b0%d0%b2%d0%b8-%d1%82%d0%be%d1%82%d0%be-%d0%bd-%d0%b8-%d0%ba%d1%83-%d0%ba%d1%83-%d0%b1%d0%b5%d0%bd%d0%b4-%d0%b7%d0%b0%d0%b5%d0%b4%d0%bd%d0%be/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/%d1%81%d0%bb%d0%b0%d0%b2%d0%b8-%d1%82%d0%be%d1%82%d0%be-%d0%bd-%d0%b8-%d0%ba%d1%83-%d0%ba%d1%83-%d0%b1%d0%b5%d0%bd%d0%b4-%d0%b7%d0%b0%d0%b5%d0%b4%d0%bd%d0%be/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/01/20200101_KLIP_HIMN2.jpg\",\n" +
            "                    \"videoTitle\": \"Химн на Република България\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/%d1%85%d0%b8%d0%bc%d0%bd-%d0%bd%d0%b0-%d1%80%d0%b5%d0%bf%d1%83%d0%b1%d0%bb%d0%b8%d0%ba%d0%b0-%d0%b1%d1%8a%d0%bb%d0%b3%d0%b0%d1%80%d0%b8%d1%8f/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/%d1%85%d0%b8%d0%bc%d0%bd-%d0%bd%d0%b0-%d1%80%d0%b5%d0%bf%d1%83%d0%b1%d0%bb%d0%b8%d0%ba%d0%b0-%d0%b1%d1%8a%d0%bb%d0%b3%d0%b0%d1%80%d0%b8%d1%8f/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2019/11/20191211_LILI_GUMZATA.jpg\",\n" +
            "                    \"videoTitle\": \"Лилия и Гъмзата – „Нека ни мразят“\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/%d0%bb%d0%b8%d0%bb%d0%b8%d1%8f-%d0%b8-%d0%b3%d1%8a%d0%bc%d0%b7%d0%b0%d1%82%d0%b0-%d0%bd%d0%b5%d0%ba%d0%b0-%d0%bd%d0%b8-%d0%bc%d1%80%d0%b0%d0%b7%d1%8f%d1%82/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/%d0%bb%d0%b8%d0%bb%d0%b8%d1%8f-%d0%b8-%d0%b3%d1%8a%d0%bc%d0%b7%d0%b0%d1%82%d0%b0-%d0%bd%d0%b5%d0%ba%d0%b0-%d0%bd%d0%b8-%d0%bc%d1%80%d0%b0%d0%b7%d1%8f%d1%82/\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2019/11/20191211_KLIPP_BORIS_SHOUTO.jpg\",\n" +
            "                    \"videoTitle\": \"Борис Солтарийски – „Шоуто ще продължи“\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/%d0%b1%d0%be%d1%80%d0%b8%d1%81-%d1%81%d0%be%d0%bb%d1%82%d0%b0%d1%80%d0%b8%d0%b9%d1%81%d0%ba%d0%b8-%d1%88%d0%be%d1%83%d1%82%d0%be-%d1%89%d0%b5-%d0%bf%d1%80%d0%be%d0%b4%d1%8a%d0%bb%d0%b6%d0%b8/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/%d0%b1%d0%be%d1%80%d0%b8%d1%81-%d1%81%d0%be%d0%bb%d1%82%d0%b0%d1%80%d0%b8%d0%b9%d1%81%d0%ba%d0%b8-%d1%88%d0%be%d1%83%d1%82%d0%be-%d1%89%d0%b5-%d0%bf%d1%80%d0%be%d0%b4%d1%8a%d0%bb%d0%b6%d0%b8/\"\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": \"<a href=\\\"https://sedemosmi.tv/акценти/\\\">акценти</a>\",\n" +
            "            \"title\": \"<a href=\\\"https://sedemosmi.tv/акценти/\\\">Акценти</a>\",\n" +
            "            \"pageViews\": [\n" +
            "                {\n" +
            "                    \"videoCover\": \"https://sedemosmi.tv/wp-content/uploads/2020/05/3_1-14.jpg\",\n" +
            "                    \"videoTitle\": \"Извънреден брифинг ли трябва да дава сега?\",\n" +
            "                    \"nextPageLink\": \"https://sedemosmi.tv/%d0%b8%d0%b7%d0%b2%d1%8a%d0%bd%d1%80%d0%b5%d0%b4%d0%b5%d0%bd-%d0%b1%d1%80%d0%b8%d1%84%d0%b8%d0%bd%d0%b3-%d0%bb%d0%b8-%d1%82%d1%80%d1%8f%d0%b1%d0%b2%d0%b0-%d0%b4%d0%b0-%d0%b4%d0%b0%d0%b2%d0%b0-%d1%81/\",\n" +
            "                    \"videoUrlLink\": \"https://sedemosmi.tv/%d0%b8%d0%b7%d0%b2%d1%8a%d0%bd%d1%80%d0%b5%d0%b4%d0%b5%d0%bd-%d0%b1%d1%80%d0%b8%d1%84%d0%b8%d0%bd%d0%b3-%d0%bb%d0%b8-%d1%82%d1%80%d1%8f%d0%b1%d0%b2%d0%b0-%d0%b4%d0%b0-%d0%b4%d0%b0%d0%b2%d0%b0-%d1%81/\"\n" +
            "                }\n" +
            "            ]\n" +
            "        }\n" +
            "    ]\n" +
            "}"

    override fun fetchHomepage(
        token: String
    ): Call<HomepageResponse> {
        val mapper = jacksonObjectMapper()
        val serviceResponse = mapper.readValue(data, HomepageResponse::class.java)
        return delegate.returningResponse(serviceResponse).fetchHomepage(BuildConfig.SEDEM_OSMI_APPLICATION_KEY);
    }

    override fun extractUrlFromItem(videoUrlBody: ExtractVideoUrlBody): Call<VideoPageResponse> {
        TODO("Not yet implemented")
    }
}