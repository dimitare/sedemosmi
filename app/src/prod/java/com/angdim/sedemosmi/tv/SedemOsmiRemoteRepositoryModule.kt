package com.angdim.sedemosmi.tv

import com.angdim.sedemosmi.tv.data.services.SedemOsmiDataSource
import com.angdim.sedemosmi.tv.data.services.SedemOsmiRepository
import com.angdim.sedemosmi.tv.data.services.SedemOsmiRepositoryImpl
import com.angdim.sedemosmi.tv.data.services.remote.SedemOsmiRemoteDataSource
import com.angdim.sedemosmi.tv.data.services.remote.SedemOsmiServiceApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class SedemOsmiRemoteRepositoryModule {
    @Provides
    @Singleton
    fun providesService(
        sedemOsmiSource: SedemOsmiDataSource
    ): SedemOsmiRepository {
        return SedemOsmiRepositoryImpl(
            sedemOsmiSource
        )
    }

    @Provides
    fun provideDemoDataSource(sedemOsmiService: SedemOsmiServiceApi): SedemOsmiDataSource {
        return SedemOsmiRemoteDataSource(sedemOsmiService)
    }

    @Provides
    fun providesDemoService(retrofit: Retrofit): SedemOsmiServiceApi {
        return retrofit.create(SedemOsmiServiceApi::class.java)
    }
}