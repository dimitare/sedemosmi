package com.angdim.sedemosmi.tv.data.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class LiveVideo(
    var videoCover : String? = null,
    var videoTitle : String? = null,
    var videoUrl : String? = null
)