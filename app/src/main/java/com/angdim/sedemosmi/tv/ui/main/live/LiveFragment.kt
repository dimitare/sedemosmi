package com.angdim.sedemosmi.tv.ui.main.live

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.angdim.sedemosmi.tv.R
import com.angdim.sedemosmi.tv.ui.base.fragment.VideoOrientation
import com.angdim.sedemosmi.tv.utils.hideSystemNavigation
import com.angdim.sedemosmi.tv.utils.showSystemNavigation

class LiveFragment: Fragment(), VideoOrientation {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.live_fragment, container, false)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        orientVideoDescriptionFragment(newConfig.orientation)
    }

    override fun orientVideoDescriptionFragment(orientation: Int) {
        // Hide the extra content when in landscape so the video is as large as possible.
        val fragmentManager = childFragmentManager
        val extraContentFragment = fragmentManager.findFragmentById(R.id.contentVideo)
        if (extraContentFragment != null) {
            val fragmentTransaction =
                fragmentManager.beginTransaction()
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                fragmentTransaction.hide(extraContentFragment)
                this.hideSystemNavigation(activity)
            } else {
                fragmentTransaction.show(extraContentFragment)
                this.showSystemNavigation(activity)
            }
            fragmentTransaction.commit()
        }
    }
}