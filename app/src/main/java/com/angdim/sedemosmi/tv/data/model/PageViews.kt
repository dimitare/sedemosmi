package com.angdim.sedemosmi.tv.data.model

import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class PageViews(
    val videoCover: String,
    val videoTitle: String,
    val nextPageLink: String? = null,
    val videoUrlLink: String? = null
) : PageView, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(videoCover)
        parcel.writeString(videoTitle)
        parcel.writeString(nextPageLink)
        parcel.writeString(videoUrlLink)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PageViews> {
        override fun createFromParcel(parcel: Parcel): PageViews {
            return PageViews(parcel)
        }

        override fun newArray(size: Int): Array<PageViews?> {
            return arrayOfNulls(size)
        }
    }
}