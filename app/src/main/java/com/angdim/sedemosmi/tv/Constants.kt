package com.angdim.sedemosmi.tv

object Constants {
    internal val TYPE_KEY = "type"
    internal val ID_KEY = "id"
    internal val CAROUSEL_KEY = "carousel"
    internal const val LIVE_VIDEO_URL = BuildConfig.LIVE_VIDEO_URL
    internal const val CAROUSEL_FRAGMENT = "CarouselFragment"
    internal const val VIDEO_FRAGMENT = "VideoFragment"
    internal const val VIDEO_FRAGMENT_EXO_PLAYER = "VideoFragmentExoPlayer"
}