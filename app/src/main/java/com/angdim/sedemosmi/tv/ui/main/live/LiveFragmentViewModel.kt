package com.angdim.sedemosmi.tv.ui.main.live

import com.angdim.sedemosmi.tv.di.scope.FragmentScope
import com.angdim.sedemosmi.tv.domain.GetDetailPageUseCase
import com.angdim.sedemosmi.tv.ui.main.detail.DetailFragmentViewModel
import javax.inject.Inject

@FragmentScope
class LiveFragmentViewModel @Inject constructor(override val getDetailPageUseCase: GetDetailPageUseCase) :
    DetailFragmentViewModel(getDetailPageUseCase) {
}