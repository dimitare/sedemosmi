package com.angdim.sedemosmi.tv.utils

import android.util.Log
import com.angdim.sedemosmi.tv.Constants
import com.angdim.sedemosmi.tv.data.model.CarouselData
import com.angdim.sedemosmi.tv.data.model.Section
import com.angdim.sedemosmi.tv.data.model.Sections
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

class HomepageDeserializer : JsonDeserializer<List<Section>>() {
    override fun deserialize(jp: JsonParser?, ctxt: DeserializationContext?): List<Section> {
        val values: MutableList<Section> = mutableListOf()
        val mapper = jacksonObjectMapper()
        val nodes: JsonNode? = jp?.codec?.readTree(jp)
        nodes?.let {
            nodes.map {
                var pageViewItem: Section? = null
                if (it.has(Constants.ID_KEY)){
                    pageViewItem = when(it.path(Constants.ID_KEY).textValue()){
                        Constants.CAROUSEL_KEY -> mapper.treeToValue(it, CarouselData::class.java)
                        else -> mapper.treeToValue(it, Sections::class.java)
                    }
                    pageViewItem?.let { item->
                            values.add(item)
                    }
                }
            }
        }
        return values
    }
}