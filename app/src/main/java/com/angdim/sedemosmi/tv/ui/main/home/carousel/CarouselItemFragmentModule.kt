package com.angdim.sedemosmi.tv.ui.main.home.carousel

import androidx.fragment.app.Fragment
import com.angdim.sedemosmi.tv.di.keys.FragmentViewModelKey
import com.angdim.sedemosmi.tv.di.scope.FragmentScope
import com.angdim.sedemosmi.tv.ui.base.fragment.BaseViewModelFragmentModule
import com.angdim.sedemosmi.tv.ui.base.viewmodel.BaseViewFragmentViewModel
import com.angdim.sedemosmi.tv.ui.base.viewmodel.BaseViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [BaseViewModelFragmentModule::class])
abstract class CarouselItemFragmentModule : BaseViewModel() {
    @Binds
    @FragmentScope
    abstract fun bindFragment(fragment: CarouselItemFragment): Fragment

    @Binds
    @IntoMap
    @FragmentViewModelKey(CarouselFragmentViewModel::class)
    @FragmentScope
    abstract fun bindViewModel(viewModel: CarouselItemFragmentViewModel): BaseViewFragmentViewModel
}