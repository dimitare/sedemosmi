package com.angdim.sedemosmi.tv.ui.main.home.carousel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.angdim.sedemosmi.tv.databinding.CarouselFragmentLayoutBinding
import com.angdim.sedemosmi.tv.ui.base.fragment.BaseDaggerFragment
import java.util.*
import kotlin.concurrent.fixedRateTimer

class CarouselFragment : BaseDaggerFragment<CarouselFragmentViewModel>() {
    override val mViewModel by viewModels<CarouselFragmentViewModel>()
    lateinit var mBinding: CarouselFragmentLayoutBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = CarouselFragmentLayoutBinding.inflate(
            LayoutInflater.from(inflater.context), container, false
        )
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.currentCarouselIndex.observe(viewLifecycleOwner, Observer {
            mBinding.carouselPager.currentItem = it
        })
        timerStart()
    }


    override fun onStop() {
        super.onStop()
        timerStop()
    }

    private lateinit var mTimer: Timer

    private fun timerStop() {
        if (::mTimer.isInitialized) {
            mTimer.cancel()
        }
    }

    private fun timerStart() {
        mTimer = fixedRateTimer(period = 5000, daemon = true, initialDelay = 5000) {
            mViewModel.updateCarousel(mBinding.carouselPager.adapter?.itemCount ?: 0)
        }
    }
}