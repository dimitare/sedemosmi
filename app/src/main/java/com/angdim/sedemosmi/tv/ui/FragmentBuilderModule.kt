package com.angdim.sedemosmi.tv.ui

import com.angdim.sedemosmi.tv.di.scope.FragmentScope
import com.angdim.sedemosmi.tv.ui.main.detail.DetailFragment
import com.angdim.sedemosmi.tv.ui.main.detail.DetailFragmentModule
import com.angdim.sedemosmi.tv.ui.main.home.HomeFragment
import com.angdim.sedemosmi.tv.ui.main.home.carousel.CarouselFragmentModule
import com.angdim.sedemosmi.tv.ui.main.home.carousel.CarouselItemFragment
import com.angdim.sedemosmi.tv.ui.main.home.HomeFragmentModule
import com.angdim.sedemosmi.tv.ui.main.home.carousel.CarouselFragment
import com.angdim.sedemosmi.tv.ui.main.home.carousel.CarouselItemFragmentModule
import com.angdim.sedemosmi.tv.ui.main.live.LiveFragmentExoPlayer
import com.angdim.sedemosmi.tv.ui.main.live.LiveFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {
    @ContributesAndroidInjector(modules = [HomeFragmentModule::class])
    @FragmentScope
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector(modules = [CarouselFragmentModule::class])
    @FragmentScope
    abstract fun contributeCarouselFragment(): CarouselFragment

    @ContributesAndroidInjector(modules = [CarouselItemFragmentModule::class])
    @FragmentScope
    abstract fun contributeCarouselItemFragment(): CarouselItemFragment

    @ContributesAndroidInjector(modules = [DetailFragmentModule::class])
    @FragmentScope
    abstract fun contributeDetailFragment(): DetailFragment

    @ContributesAndroidInjector(modules = [LiveFragmentModule::class])
    @FragmentScope
    abstract fun contributeLLiveFragmentExoPlayer(): LiveFragmentExoPlayer
}