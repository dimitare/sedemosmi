package com.angdim.sedemosmi.tv.data.services.remote

data class ExtractVideoUrlBody(val token: String, val movieUrl: String)