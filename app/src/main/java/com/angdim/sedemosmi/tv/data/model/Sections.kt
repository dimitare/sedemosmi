package com.angdim.sedemosmi.tv.data.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class Sections (
	override val id : String,
	override val title : String,
	override val pageViews : List<PageViews>? = null
): Section