package com.angdim.sedemosmi.tv.data.services

import com.angdim.sedemosmi.tv.data.model.HomepageResponse
import com.angdim.sedemosmi.tv.data.model.VideoPageResponse
import com.angdim.sedemosmi.tv.data.services.remote.Status

interface SedemOsmiRepository {
    suspend fun loadHomepage(): Status<HomepageResponse>
    suspend fun extractUrlFromItem(videoUrl: String): Status<VideoPageResponse>
}