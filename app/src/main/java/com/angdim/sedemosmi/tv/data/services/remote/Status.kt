package com.angdim.sedemosmi.tv.data.services.remote

sealed class Status<out R>{
    data class Success<out T>(val data: T) : Status<T>()
    data class Error(val exception: Exception) : Status<Nothing>()
    object Loading : Status<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
            Loading -> "Loading"
        }
    }
}

val Status<*>.succeeded
    get() = this is Status.Success && data != null