package com.angdim.sedemosmi.tv.di.keys

import com.angdim.sedemosmi.tv.ui.base.viewmodel.BaseViewFragmentViewModel
import dagger.MapKey
import kotlin.reflect.KClass

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
@MustBeDocumented
annotation class FragmentViewModelKey(val value: KClass<out BaseViewFragmentViewModel>)