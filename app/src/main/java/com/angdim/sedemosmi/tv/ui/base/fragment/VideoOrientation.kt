package com.angdim.sedemosmi.tv.ui.base.fragment

interface VideoOrientation {
    fun orientVideoDescriptionFragment(orientation: Int)
}