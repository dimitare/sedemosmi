package com.angdim.sedemosmi.tv.ui.main.home

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.angdim.sedemosmi.tv.R
import com.angdim.sedemosmi.tv.databinding.CarouselLayoutBinding
import com.angdim.sedemosmi.tv.databinding.HomeItemLayoutBinding
import com.angdim.sedemosmi.tv.data.model.CarouselData
import com.angdim.sedemosmi.tv.data.model.PageViews
import com.angdim.sedemosmi.tv.data.model.Section
import com.angdim.sedemosmi.tv.ui.main.home.carousel.CarouselItemFragment
import com.angdim.sedemosmi.tv.ui.main.home.section.SectionAdapter

class HomepageAdapter(val frgm: HomeFragment) :
    RecyclerView.Adapter<HomepageAdapter.HomepageHolder>() {
    private var items: List<Section> = emptyList()

    enum class HomeItemType(val type: Int) {
        NORMAL_TYPE(0),
        CAROUSEL(1)
    }

    open class HomepageHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomepageHolder {
        return when (viewType) {
            HomeItemType.CAROUSEL.type -> {
                val holder = CarouselHolder(
                    CarouselLayoutBinding.inflate(
                        LayoutInflater.from(parent.context), parent, false
                    )
                )
                holder.binding.frgm = frgm
                holder
            }
            else -> NormalItemHolder(
                HomeItemLayoutBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is CarouselData -> HomeItemType.CAROUSEL.type
            else -> HomeItemType.NORMAL_TYPE.type
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: HomepageHolder, position: Int) {
        when (holder) {
            is CarouselHolder -> holder.bind(items[position])
            is NormalItemHolder -> holder.bind(items[position])
        }
    }

    fun update(items: List<Section>) {
        this.items = items
        notifyDataSetChanged()
    }

    private fun requestFragmentActivity(context: Context): FragmentActivity? {
        if (context is FragmentActivity) {
            return context
        }
        return null
    }

    inner class CarouselHolder(val binding: CarouselLayoutBinding) :
        HomepageHolder(binding.root) {
        fun bind(section: Section) {
            section.pageViews?.let { pages ->
                val carouselPager = binding.root.findViewById<ViewPager2>(R.id.carousel_pager)
                carouselPager?.apply {
                    rootView?.let {
                        requestFragmentActivity(it.context)?.let { activity ->
                            adapter = CarouselAdapter(activity, pages)
                        }
                    }
                }
                carouselPager?.setOnClickListener {
                    Log.w("", "")
                }
            }
            binding.executePendingBindings()
        }

        inner class CarouselAdapter(
            fm: FragmentActivity,
            val pages: List<PageViews>
        ) : FragmentStateAdapter(fm) {
            override fun getItemCount(): Int {
                return pages.size
            }

            override fun createFragment(position: Int): Fragment {
                return CarouselItemFragment.newInstance(pages[position])
            }
        }
    }

    class NormalItemHolder(
        val binding: HomeItemLayoutBinding
    ) : HomepageHolder(binding.root) {
        fun bind(section: Section) {
            binding.sectionTitle.text = section.title
            binding.content.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = SectionAdapter(section.pageViews) {
                    val direction =
                        HomeFragmentDirections.actionHomeFragmentToDetailFragment(it.videoUrlLink)
                    findNavController().navigate(direction)
                }
            }
            binding.executePendingBindings()
        }
    }

    companion object {
        @JvmStatic
        @BindingAdapter("items")
        fun RecyclerView.bindItems(items: List<Section>?) {
            val adapter = adapter as HomepageAdapter
            items?.let {
                adapter.update(items)
            }
        }
    }
}