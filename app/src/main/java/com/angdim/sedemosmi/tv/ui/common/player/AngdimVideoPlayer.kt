package com.angdim.sedemosmi.tv.ui.common.player

import android.content.Context
import android.media.MediaPlayer.OnCompletionListener
import android.util.AttributeSet
import android.widget.MediaController
import android.widget.VideoView
import java.util.*

class AngdimVideoPlayer : VideoView {
    /**
     * Interface for alerting caller of video completion.
     */
    interface OnVideoCompletedListener {
        /**
         * Called when the current video has completed playback to the end of the video.
         */
        fun onVideoCompleted()
    }

    private val mOnVideoCompletedListeners: MutableList<OnVideoCompletedListener> =
        ArrayList(1)

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyle: Int
    ) : super(context, attrs, defStyle) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
        init()
    }

    constructor(context: Context?) : super(context) {
        init()
    }

    private fun init() {
        val mediaController =
            MediaController(context)
        mediaController.setAnchorView(this)
        // Set OnCompletionListener to notify our listeners when the video is completed.
        super.setOnCompletionListener { mediaPlayer ->
            // Reset the MediaPlayer.
            // This prevents a race condition which occasionally results in the media
            // player crashing when switching between videos.
            mediaPlayer.reset()
            mediaPlayer.setDisplay(holder)
            for (listener in mOnVideoCompletedListeners) {
                listener.onVideoCompleted()
            }
        }
        // Set OnErrorListener to notify our listeners if the video errors.
        super.setOnErrorListener { mp, what, extra ->
            // Returning true signals to MediaPlayer that we handled the error. This will
            // prevent the completion handler from being called.
            true
        }
    }

    override fun setOnCompletionListener(listener: OnCompletionListener) { // The OnCompletionListener can only be implemented by SampleVideoPlayer.
        throw UnsupportedOperationException()
    }

    fun play() {
        start()
    }

    fun addVideoCompletedListener(listener: OnVideoCompletedListener) {
        mOnVideoCompletedListeners.add(listener)
    }
}