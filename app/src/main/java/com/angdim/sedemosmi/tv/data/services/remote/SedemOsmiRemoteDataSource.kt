package com.angdim.sedemosmi.tv.data.services.remote

import com.angdim.sedemosmi.tv.BuildConfig
import com.angdim.sedemosmi.tv.data.model.HomepageResponse
import com.angdim.sedemosmi.tv.data.model.VideoPageResponse
import com.angdim.sedemosmi.tv.data.services.SedemOsmiDataSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SedemOsmiRemoteDataSource (
    val mSedemOsmiServiceApi: SedemOsmiServiceApi,
    private val mIoDispatcher: CoroutineDispatcher = Dispatchers.IO
) : SedemOsmiDataSource {
    override suspend fun fetchHomepage(): Status<HomepageResponse> =
    withContext(mIoDispatcher) {
        val result = mSedemOsmiServiceApi.fetchHomepage(BuildConfig.SEDEM_OSMI_APPLICATION_KEY).execute()
        if( result.isSuccessful ){
            return@withContext Status.Success(result.body()!!)
        }
        return@withContext Status.Error(Exception(result.message()))
    }

    override suspend fun extractUrlFromItem(videoUrl: String): Status<VideoPageResponse> = withContext(mIoDispatcher) {
        val extractVideoUrlBody = ExtractVideoUrlBody(BuildConfig.SEDEM_OSMI_APPLICATION_KEY, videoUrl)
        val result = mSedemOsmiServiceApi.extractUrlFromItem(extractVideoUrlBody).execute()
        if( result.isSuccessful ){
            return@withContext Status.Success(result.body()!!)
        }
        return@withContext Status.Error(Exception(result.message()))
    }
}