package com.angdim.sedemosmi.tv.di

import com.angdim.sedemosmi.tv.SedemOsmiApplication
import com.angdim.sedemosmi.tv.di.module.ApplicationModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * Created by @Dimitare on 03/31/20.
 */
@Singleton
@Component(modules = [ApplicationModule::class])
interface AppComponent : AndroidInjector<SedemOsmiApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: SedemOsmiApplication): Builder

        fun build(): AppComponent
    }
}