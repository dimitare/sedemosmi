package com.angdim.sedemosmi.tv.data.model

import android.net.Uri

class VideoModel(val uris: Array<Uri>) {
    private var resumePosition: Long = 0
    private val extensions: Array<String>? = null
}