package com.angdim.sedemosmi.tv.ui.main.home.carousel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.angdim.sedemosmi.tv.di.scope.FragmentScope
import com.angdim.sedemosmi.tv.ui.base.viewmodel.BaseViewFragmentViewModel
import javax.inject.Inject

@FragmentScope
class CarouselFragmentViewModel @Inject constructor() : BaseViewFragmentViewModel() {

    private val _currentCarouselIndex = MutableLiveData<Int>(0)
    val currentCarouselIndex: LiveData<Int> = _currentCarouselIndex.map {
        it
    }

    fun updateCarousel(itemCount: Int) {
        var nextIndex = _currentCarouselIndex.value ?: 0
        _currentCarouselIndex.postValue(if(nextIndex < itemCount) ++nextIndex else (0))
    }
}