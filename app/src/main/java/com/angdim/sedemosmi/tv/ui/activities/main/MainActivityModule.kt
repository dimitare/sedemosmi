package com.angdim.sedemosmi.tv.ui.activities.main

import androidx.appcompat.app.AppCompatActivity
import com.angdim.sedemosmi.tv.ui.MainActivity
import com.angdim.sedemosmi.tv.ui.activities.BaseActivityModule
import com.angdim.sedemosmi.tv.ui.activities.BaseViewActivityViewModel
import com.angdim.sedemosmi.tv.di.keys.ActivityViewModelKey
import com.angdim.sedemosmi.tv.di.scope.ActivityScope
import com.angdim.sedemosmi.tv.ui.FragmentBuilderModule
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [BaseActivityModule::class, FragmentBuilderModule::class])
abstract class MainActivityModule {
    @Binds
    @ActivityScope
    abstract fun bindActivity(activity: MainActivity): AppCompatActivity

    @Binds
    @IntoMap
    @ActivityViewModelKey(MainActivityViewModel::class)
    @ActivityScope
    abstract fun bindViewModel(viewModel: MainActivityViewModel): BaseViewActivityViewModel
}