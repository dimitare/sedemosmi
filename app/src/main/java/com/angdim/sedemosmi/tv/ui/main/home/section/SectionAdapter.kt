package com.angdim.sedemosmi.tv.ui.main.home.section

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.angdim.sedemosmi.tv.data.model.PageViews
import com.angdim.sedemosmi.tv.databinding.NewSectionLayoutBinding
import com.angdim.sedemosmi.tv.utils.SectionItemDiffUtilCallback

class SectionAdapter(var mItems: List<PageViews>? = emptyList(), val onSectionClick: (PageViews) -> Unit) : RecyclerView.Adapter<SectionAdapter.SectionHolder>() {

    open class SectionHolder(val binding: NewSectionLayoutBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SectionHolder {
        return SectionHolder(
            NewSectionLayoutBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount(): Int = mItems?.size ?: 0

    override fun onBindViewHolder(holder: SectionHolder, position: Int) {
        val item = mItems!![position]
        holder.binding.section = item
        holder.binding.executePendingBindings()
        holder.itemView.setOnClickListener {
            onSectionClick(item)
        }
    }

    fun setData(items: Collection<PageViews>) {
        val oldList: List<PageViews> = mItems ?: listOf()
        mItems = items as MutableList<PageViews>
        val diffResult =
            DiffUtil.calculateDiff(SectionItemDiffUtilCallback(items, oldList), true)
        diffResult.dispatchUpdatesTo(this)
    }
}