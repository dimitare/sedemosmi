package com.angdim.sedemosmi.tv.ui.common.viewmodel

import androidx.lifecycle.ViewModel
import com.angdim.sedemosmi.tv.di.scope.FragmentScope
import com.angdim.sedemosmi.tv.ui.base.viewmodel.BaseViewFragmentViewModel
import com.angdim.sedemosmi.tv.ui.base.viewmodel.factory.BaseViewModelFactory
import javax.inject.Inject
import javax.inject.Provider

@FragmentScope
class FragmentViewModelFactory @Inject constructor(
    creators: Map<Class<out BaseViewFragmentViewModel>,
            @JvmSuppressWildcards Provider<BaseViewFragmentViewModel>>
) : BaseViewModelFactory(creators as Map<Class<out ViewModel>, Provider<ViewModel>>)