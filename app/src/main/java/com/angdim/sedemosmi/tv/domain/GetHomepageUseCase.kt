package com.angdim.sedemosmi.tv.domain

import com.angdim.sedemosmi.tv.data.model.HomepageResponse
import com.angdim.sedemosmi.tv.data.services.SedemOsmiRepository
import com.angdim.sedemosmi.tv.data.services.remote.Status
import javax.inject.Inject

class GetHomepageUseCase @Inject constructor(val mSedemOsmiRepository: SedemOsmiRepository){
    suspend operator fun invoke(): Status<HomepageResponse> {
        return mSedemOsmiRepository.loadHomepage()
    }
}