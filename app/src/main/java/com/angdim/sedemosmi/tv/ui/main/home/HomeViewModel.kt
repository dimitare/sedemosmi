package com.angdim.sedemosmi.tv.ui.main.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.angdim.sedemosmi.tv.Event

class HomeViewModel(
) : ViewModel() {
    private val _openTaskEvent = MutableLiveData<Event<String>>()
    val openLiveEvent: LiveData<Event<String>> = _openTaskEvent

    private val _dataLoading = MutableLiveData<Boolean>()
    val dataLoading: LiveData<Boolean> = _dataLoading

}