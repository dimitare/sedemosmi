package com.angdim.sedemosmi.tv.ui.main.detail

import androidx.fragment.app.Fragment
import com.angdim.sedemosmi.tv.di.keys.FragmentViewModelKey
import com.angdim.sedemosmi.tv.di.scope.FragmentScope
import com.angdim.sedemosmi.tv.ui.base.fragment.BaseViewModelFragmentModule
import com.angdim.sedemosmi.tv.ui.base.viewmodel.BaseViewFragmentViewModel
import com.angdim.sedemosmi.tv.ui.base.viewmodel.BaseViewModel
import com.angdim.sedemosmi.tv.ui.common.fragment.ContentFragment
import com.angdim.sedemosmi.tv.ui.common.fragment.VideoFragmentExoPlayer
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = [BaseViewModelFragmentModule::class])
abstract class DetailFragmentModule : BaseViewModel() {
    @Binds
    @FragmentScope
    abstract fun bindFragment(fragment: DetailFragment): Fragment

    @ContributesAndroidInjector
    abstract fun contributeContentFragment(): ContentFragment

    @ContributesAndroidInjector
    abstract fun contributeVideoFragmentExoPlayer(): VideoFragmentExoPlayer

    @Binds
    @IntoMap
    @FragmentViewModelKey(DetailFragmentViewModel::class)
    @FragmentScope
    abstract fun bindViewModel(viewModel: DetailFragmentViewModel): BaseViewFragmentViewModel
}