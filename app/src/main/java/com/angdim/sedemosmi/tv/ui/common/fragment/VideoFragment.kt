package com.angdim.sedemosmi.tv.ui.common.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.angdim.sedemosmi.tv.R
import com.angdim.sedemosmi.tv.ui.common.player.AngdimVideoPlayer

open class VideoFragment: Fragment() {
    private var mVideoPlayer: AngdimVideoPlayer? = null
    private var mPlayButton: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView: View =
            inflater.inflate(R.layout.fragment_video, container, false)
        initUi(rootView)
        return rootView
    }

    private fun initUi(rootView: View) {
        mVideoPlayer =
            rootView.findViewById<View>(R.id.videoPlayer) as AngdimVideoPlayer
        mPlayButton =
            rootView.findViewById(R.id.playButton) as View
        mPlayButton?.setOnClickListener {
            playVideo(getString(R.string.content_url))
        }
    }

    fun playVideo(videoUrl: String){
        mVideoPlayer?.setVideoPath(videoUrl)
        mVideoPlayer?.play()
        mPlayButton?.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
        mVideoPlayer?.play()
    }

    override fun onPause() {
        super.onPause()
        mVideoPlayer?.pause()
    }
}