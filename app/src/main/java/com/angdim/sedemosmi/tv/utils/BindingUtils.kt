package com.angdim.sedemosmi.tv.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.angdim.sedemosmi.tv.data.model.PageViews
import com.angdim.sedemosmi.tv.ui.main.home.section.SectionAdapter
import com.bumptech.glide.Glide

@BindingAdapter("sectionData")
fun <T> setRecyclerViewProperties(
    recyclerView: RecyclerView,
    items: Collection<PageViews>?
) {
    items?.let {
        (recyclerView.adapter as? SectionAdapter)?.setData(items)
        recyclerView.adapter?.notifyDataSetChanged()
    }
}

@BindingAdapter("app:coverUrl")
fun updateAvatarImage(view: ImageView, coverUrl: String?) {
    coverUrl?.let {
        Glide.with(view).load(it)/*.placeholder(placeholder)*/
            .into(view)
    }
}
