package com.angdim.sedemosmi.tv.utils

import android.content.Context
import com.google.android.exoplayer2.database.DatabaseProvider
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.upstream.cache.*
import java.io.File

object CashManager {
    private val DOWNLOAD_CONTENT_DIRECTORY = "downloads"

    private var downloadCache: Cache? = null
    @Synchronized
    private fun getDownloadCache(context: Context, externalFilesDir: File?, filesDir: File?): Cache? {
        if (downloadCache == null) {
            val downloadContentDirectory = File(
                getDownloadDirectory(externalFilesDir, filesDir),
                DOWNLOAD_CONTENT_DIRECTORY
            )
            downloadCache =
                SimpleCache(downloadContentDirectory, NoOpCacheEvictor(), getDatabaseProvider(context))
        }
        return downloadCache
    }

    private var downloadDirectory: File? = null
    private fun getDownloadDirectory(externalFilesDir: File?, filesDir: File?): File? {
        if (downloadDirectory == null) {
            downloadDirectory = externalFilesDir
            if (downloadDirectory == null) {
                downloadDirectory = filesDir
            }
        }
        return downloadDirectory
    }

    private var databaseProvider: DatabaseProvider? = null

    private fun getDatabaseProvider(context: Context): DatabaseProvider? {
        if (databaseProvider == null) {
            databaseProvider = ExoDatabaseProvider(context)
        }
        return databaseProvider
    }

    /** Returns a [DataSource.Factory].  */
    fun buildDataSourceFactory(context: Context, userAgent: String?): DataSource.Factory {
        val upstreamFactory =
            DefaultDataSourceFactory(context, buildHttpDataSourceFactory(userAgent))
        return buildReadOnlyCacheDataSource(
            upstreamFactory,
            getDownloadCache(context, context.getExternalFilesDir(null), context.filesDir)
        )
    }

    /** Returns a [HttpDataSource.Factory].  */
    private fun buildHttpDataSourceFactory(userAgent: String?): HttpDataSource.Factory? {
        return DefaultHttpDataSourceFactory(userAgent)
    }

    private fun buildReadOnlyCacheDataSource(
        upstreamFactory: DataSource.Factory?,
        cache: Cache?
    ): CacheDataSourceFactory {
        return CacheDataSourceFactory(
            cache,
            upstreamFactory,
            FileDataSource.Factory(),  /* cacheWriteDataSinkFactory= */
            null,
            CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,  /* eventListener= */
            null
        )
    }
}