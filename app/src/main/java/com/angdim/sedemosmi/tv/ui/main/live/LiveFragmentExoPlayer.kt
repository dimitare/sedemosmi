package com.angdim.sedemosmi.tv.ui.main.live

import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.angdim.sedemosmi.tv.R
import com.angdim.sedemosmi.tv.databinding.LiveFragmentExoPlayerBinding
import com.angdim.sedemosmi.tv.ui.base.fragment.BaseDaggerFragment
import com.angdim.sedemosmi.tv.ui.base.fragment.VideoOrientation
import com.angdim.sedemosmi.tv.ui.common.fragment.VideoFragmentExoPlayer
import com.angdim.sedemosmi.tv.ui.main.detail.DetailFragmentViewModel
import com.angdim.sedemosmi.tv.utils.Utils
import com.angdim.sedemosmi.tv.utils.findFragmentByTag
import com.angdim.sedemosmi.tv.utils.hideSystemNavigation
import com.angdim.sedemosmi.tv.utils.showSystemNavigation

class LiveFragmentExoPlayer: BaseDaggerFragment<DetailFragmentViewModel>(), VideoOrientation {
    override val mViewModel by viewModels<DetailFragmentViewModel>()
    lateinit var mDataBinding: LiveFragmentExoPlayerBinding

    override fun orientVideoDescriptionFragment(orientation: Int) {
        val fragmentManager = childFragmentManager
        val extraContentFragment = fragmentManager.findFragmentById(R.id.contentVideo)
        if (extraContentFragment != null) {
            val fragmentTransaction =
                fragmentManager.beginTransaction()
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                fragmentTransaction.hide(extraContentFragment)
                this.hideSystemNavigation(activity)
            } else {
                fragmentTransaction.show(extraContentFragment)
                this.showSystemNavigation(activity)
            }
            fragmentTransaction.commit()
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        orientVideoDescriptionFragment(newConfig.orientation)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mDataBinding = LiveFragmentExoPlayerBinding.inflate(inflater, container, false).apply {
            viewModel = mViewModel
        }
        setUpActionBar()
        mDataBinding.lifecycleOwner = this
        return mDataBinding.root
    }

    private fun setUpActionBar() {
        val ab = (activity as AppCompatActivity?)?.supportActionBar
        ab?.setDisplayHomeAsUpEnabled(false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: LiveFragmentExoPlayerArgs by navArgs()
        args.videoUrlLink?.let {
            mDataBinding.viewModel?.fetchVideoByUrl(it)
        }
        mDataBinding.viewModel?.videoUrl?.observe(viewLifecycleOwner, Observer {
            val videoFragment: VideoFragmentExoPlayer? = findFragmentByTag<VideoFragmentExoPlayer>(
                Utils.getVideoPlayerType()
            )
            videoFragment?.apply {
                if (it != null) {
                    setVideoUri(listOf(Uri.parse(it)))
                }
            }
        })
        mDataBinding.viewModel?.videoUrl?.observe(viewLifecycleOwner, Observer {
            val videoFragment: VideoFragmentExoPlayer? = findFragmentByTag<VideoFragmentExoPlayer>(Utils.getVideoPlayerType())
            videoFragment?.apply {
                if (it != null) {
                    setVideoUri(listOf(Uri.parse(it)))
                }
            }
        })
        mDataBinding.viewModel?.videoTitle?.observe(viewLifecycleOwner, Observer {
            val videoFragment: VideoFragmentExoPlayer? = findFragmentByTag<VideoFragmentExoPlayer>(Utils.getVideoPlayerType())
            videoFragment?.apply {
                setVideoTitle(it)
            }
        })
    }
}