package com.angdim.sedemosmi.tv.data.services.remote

import com.angdim.sedemosmi.tv.data.model.HomepageResponse
import com.angdim.sedemosmi.tv.data.model.VideoPageResponse
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface SedemOsmiServiceApi {
    @GET("/einthusan/v2/sedemOsmi/v1/homepage")
    fun fetchHomepage(@Query("token") token: String): Call<HomepageResponse>


    @POST("/einthusan/v2/sedemOsmi/v1/excractUrl")
    fun extractUrlFromItem(@Body videoUrlBody: ExtractVideoUrlBody): Call<VideoPageResponse>
}