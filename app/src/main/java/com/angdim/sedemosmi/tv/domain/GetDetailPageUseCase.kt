package com.angdim.sedemosmi.tv.domain

import com.angdim.sedemosmi.tv.data.model.VideoPageResponse
import com.angdim.sedemosmi.tv.data.services.SedemOsmiRepository
import com.angdim.sedemosmi.tv.data.services.remote.Status
import javax.inject.Inject

class GetDetailPageUseCase @Inject constructor(val mSedemOsmiRepository: SedemOsmiRepository){
    suspend operator fun invoke(videoUrlLink: String): Status<VideoPageResponse> {
        return mSedemOsmiRepository.extractUrlFromItem(videoUrlLink)
    }
}