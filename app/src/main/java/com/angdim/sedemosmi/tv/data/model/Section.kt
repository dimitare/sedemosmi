package com.angdim.sedemosmi.tv.data.model

interface Section {
    val id : String
    val title : String
    val pageViews : List<PageViews>?
}