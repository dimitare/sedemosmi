package com.angdim.sedemosmi.tv.data.services

import com.angdim.sedemosmi.tv.data.model.HomepageResponse
import com.angdim.sedemosmi.tv.data.model.VideoPageResponse
import com.angdim.sedemosmi.tv.data.services.remote.Status

interface SedemOsmiDataSource {
    suspend fun fetchHomepage(): Status<HomepageResponse>
    suspend fun extractUrlFromItem(videoUrl: String): Status<VideoPageResponse>
}