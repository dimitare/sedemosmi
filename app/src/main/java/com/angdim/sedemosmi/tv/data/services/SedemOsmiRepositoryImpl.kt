package com.angdim.sedemosmi.tv.data.services

import android.util.Log
import com.angdim.sedemosmi.tv.data.model.HomepageResponse
import com.angdim.sedemosmi.tv.data.model.VideoPageResponse
import com.angdim.sedemosmi.tv.data.services.remote.Status
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class SedemOsmiRepositoryImpl(
    val mSedemOsmiSource: SedemOsmiDataSource,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : SedemOsmiRepository, CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    override suspend fun loadHomepage(): Status<HomepageResponse> {
        return withContext(coroutineContext) {
            val result = loadHomepageFromRemoteDataSource()
            when (result) {
                is Status.Success<HomepageResponse> -> {
                    result.data.let {
                        Log.w("Dimitare", "Status.Success<HomepageResponse>")
                    }
                }
                else -> {
                }
            }
            return@withContext result
        }
    }

    private suspend fun loadHomepageFromRemoteDataSource(): Status<HomepageResponse> {
        return when (val result = mSedemOsmiSource.fetchHomepage()) {
            is Status.Error -> result
            is Status.Success<HomepageResponse> -> result
            else -> throw IllegalStateException()
        }
    }

    override suspend fun extractUrlFromItem(videoUrl: String): Status<VideoPageResponse> {
        return withContext(ioDispatcher) {
            val result = extractUrlFromItemRemotely(videoUrl)
            when (result) {
                is Status.Success<VideoPageResponse> -> {
                    result.data.let {
                        Log.w("Dimitare", "Status.Success<VideoUrl>")
                    }
                }
                else -> {
                }
            }
            return@withContext result
        }
    }

    private suspend fun extractUrlFromItemRemotely(videoUrl: String): Status<VideoPageResponse> {
        return when (val result = mSedemOsmiSource.extractUrlFromItem(videoUrl)) {
            is Status.Error -> result
            is Status.Success<VideoPageResponse> -> result
            else -> throw IllegalStateException()
        }
    }
}