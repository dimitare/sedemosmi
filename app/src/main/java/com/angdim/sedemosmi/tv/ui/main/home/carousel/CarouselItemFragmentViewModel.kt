package com.angdim.sedemosmi.tv.ui.main.home.carousel

import com.angdim.sedemosmi.tv.di.scope.FragmentScope
import com.angdim.sedemosmi.tv.ui.base.viewmodel.BaseViewFragmentViewModel
import javax.inject.Inject

@FragmentScope
class CarouselItemFragmentViewModel @Inject constructor() : BaseViewFragmentViewModel() {
}