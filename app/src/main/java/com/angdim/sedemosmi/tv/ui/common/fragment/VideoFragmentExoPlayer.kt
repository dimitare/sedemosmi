package com.angdim.sedemosmi.tv.ui.common.fragment

import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.angdim.sedemosmi.tv.BuildConfig
import com.angdim.sedemosmi.tv.R
import com.angdim.sedemosmi.tv.databinding.FragmentVideoExoPlayerBinding
import com.angdim.sedemosmi.tv.ui.base.fragment.BaseDaggerFragment
import com.angdim.sedemosmi.tv.ui.main.detail.DetailFragmentViewModel
import com.angdim.sedemosmi.tv.ui.main.live.LiveFragmentViewModel
import com.angdim.sedemosmi.tv.utils.CashManager.buildDataSourceFactory
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.DefaultRenderersFactory.ExtensionRendererMode
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.drm.DrmSessionManager
import com.google.android.exoplayer2.drm.ExoMediaCrypto
import com.google.android.exoplayer2.metadata.Metadata
import com.google.android.exoplayer2.metadata.MetadataOutput
import com.google.android.exoplayer2.source.*
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource
import com.google.android.exoplayer2.text.Cue
import com.google.android.exoplayer2.text.TextOutput
import com.google.android.exoplayer2.trackselection.*
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector.ParametersBuilder
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.EventLogger
import com.google.android.exoplayer2.util.Util

class VideoFragmentExoPlayer: BaseDaggerFragment<DetailFragmentViewModel>(), PlaybackPreparer,
    MetadataOutput, TextOutput {
    override val mViewModel by viewModels<DetailFragmentViewModel>()
    lateinit var mDataBinding: FragmentVideoExoPlayerBinding

    private var mShouldAutoPlay = false
    private var mResumeWindow = 0
    private var mResumePosition: Long = 0
    private var mFullscreenIb: ImageButton? = null
    private var mVideoTitle: TextView? = null
    private var isFullscreen = false
    private var mPlayer: SimpleExoPlayer? = null
    private var mediaSource: MediaSource? = null
    private lateinit var mDataSourceFactory: DataSource.Factory
    private var mUserAgent: String? = null
    private val ABR_ALGORITHM_EXTRA = "abr_algorithm"
    private val ABR_ALGORITHM_DEFAULT = "default"
    private val ABR_ALGORITHM_RANDOM = "random"
    private var trackSelector: DefaultTrackSelector? = null
    private val KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters"
    private val KEY_WINDOW = "window"
    private val KEY_POSITION = "position"
    private val KEY_AUTO_PLAY = "auto_play"
    private var startAutoPlay = false
    private var startWindow = 0
    private var startPosition: Long = 0

    private var mPlaybackStateListener: PlayerEventListener? = null

    private lateinit var trackSelectorParameters: DefaultTrackSelector.Parameters

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mUserAgent = Util.getUserAgent(requireContext(), "ExoPlayerDemo")
        mDataSourceFactory = buildDataSourceFactory()
        mShouldAutoPlay = false
        clearResumePosition()

        if (savedInstanceState != null) {
            trackSelectorParameters =
                savedInstanceState.getParcelable(KEY_TRACK_SELECTOR_PARAMETERS)
                    ?: ParametersBuilder( /* context= */requireContext()).build()
            startAutoPlay =
                savedInstanceState.getBoolean(KEY_AUTO_PLAY)
            startWindow =
                savedInstanceState.getInt(KEY_WINDOW)
            startPosition =
                savedInstanceState.getLong(KEY_POSITION)
        } else {
            trackSelectorParameters = buildDefaultTrackSelectorParameters()
            buildDefaultTrackSelectorParameters()
            clearStartPosition()
        }
    }

    private fun buildDefaultTrackSelectorParameters(): DefaultTrackSelector.Parameters {
        val builder = ParametersBuilder( /* context= */requireContext())
        val tunneling = false
        if (Util.SDK_INT >= 21 && tunneling) {
            builder.setTunnelingAudioSessionId(
                C.generateAudioSessionIdV21( /* context= */requireContext())
            )
        }
        return builder.build()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mDataBinding = FragmentVideoExoPlayerBinding.inflate(inflater, container, false).apply {
            viewModel = mViewModel
        }
        initView()
        return mDataBinding.root
    }

    override fun onStart() {
        super.onStart()
        if (Util.SDK_INT > 23) {
            initializePlayer()
        }
    }

    override fun onPause() {
        super.onPause()
        if (Util.SDK_INT <= 23) {
            mDataBinding.playerView.onPause()
            releasePlayer()
        }
    }

    override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > 23) {
            mDataBinding.playerView.onPause()
            releasePlayer()
        }
    }

    private fun initView() {
        mVideoTitle = mDataBinding.root.findViewById(R.id.video_title)
        mFullscreenIb = mDataBinding.root.findViewById(R.id.exo_full_screen)
        mFullscreenIb?.setOnClickListener { view: View? -> onFullScreenClicked() }
    }

    fun setVideoUri(urisList: List<Uri>) {
        mediaSource = createTopLevelMediaSource(urisList) ?: return
        val haveResumePosition = mResumeWindow != C.INDEX_UNSET
        if (haveResumePosition) {
            mPlayer?.seekTo(mResumeWindow, mResumePosition)
        }
        mediaSource?.let {
            mPlayer?.prepare(it, !haveResumePosition, false)
        }
    }

    fun setVideoTitle(title: String?) {
        mVideoTitle?.text = title
    }

    fun showHideView(v: View, show: Boolean) {
        if (show) {
            v.visibility = View.VISIBLE
        } else {
            v.visibility = View.GONE
        }
    }

    private fun onFullScreenClicked() {
        isFullscreen = !isFullscreen
        if (isFullscreen) {
            mFullscreenIb?.setImageResource(R.drawable.ic_fullscreen_exit)
            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
        } else {
            mFullscreenIb?.setImageResource(R.drawable.ic_fullscreen)
            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
    }

    private fun clearResumePosition() {
        mResumeWindow = C.INDEX_UNSET
        mResumePosition = C.TIME_UNSET
    }

    private fun clearStartPosition() {
        startAutoPlay = true
        startWindow = C.INDEX_UNSET
        startPosition = C.TIME_UNSET
    }

    // Internal methods
    private fun initializePlayer() {
        if (mPlayer == null) {
            mediaSource = createTopLevelMediaSource(listOf()) ?: return
            val trackSelectionFactory: TrackSelection.Factory
            val abrAlgorithm = ABR_ALGORITHM_DEFAULT
            trackSelectionFactory =
                when {
                    ABR_ALGORITHM_DEFAULT == abrAlgorithm -> {
                        AdaptiveTrackSelection.Factory()
                    }
                    ABR_ALGORITHM_RANDOM == abrAlgorithm -> {
                        RandomTrackSelection.Factory()
                    }
                    else -> {
                        showToast(R.string.error_unrecognized_abr_algorithm)
                        activity?.finish()
                        return
                    }
                }
            val preferExtensionDecoders = false
            val renderersFactory: RenderersFactory = buildRenderersFactory(preferExtensionDecoders)
            trackSelector =
                DefaultTrackSelector( /* context= */requireContext(), trackSelectionFactory)
            trackSelector?.parameters = trackSelectorParameters
            //lastSeenTrackGroupArray = null
            trackSelector?.let { trackSelector ->
                mPlayer = SimpleExoPlayer.Builder( /* context= */requireContext(), renderersFactory)
                    .setTrackSelector(trackSelector)
                    .build()
                mPlayer?.let { player ->
                    mPlaybackStateListener = PlayerEventListener()
                    mPlaybackStateListener?.let {
                        player.addListener(it)
                    }
                    player.addMetadataOutput(this@VideoFragmentExoPlayer)
                    player.addTextOutput(this@VideoFragmentExoPlayer)
                    player.setAudioAttributes(
                        AudioAttributes.DEFAULT,  /* handleAudioFocus= */
                        true
                    )
                    player.playWhenReady = startAutoPlay
                    player.addAnalyticsListener(EventLogger(trackSelector))
                    mDataBinding.playerView.player = player
                    mDataBinding.playerView.setPlaybackPreparer(this)
//                debugViewHelper = DebugTextViewHelper(player, debugTextView)
//                debugViewHelper.start()
//                if (adsLoader != null) {
//                    adsLoader.setPlayer(player)
//                }
                }
            }
        }
        val haveStartPosition = startWindow != C.INDEX_UNSET
        if (haveStartPosition) {
            mPlayer?.seekTo(startWindow, startPosition)
        }
        mediaSource?.let {
            mPlayer?.prepare(it, !haveStartPosition, false)
        }
    }

    override fun onMetadata(metadata: Metadata) {
    }

    override fun onCues(cues: MutableList<Cue>) {
    }

    private fun showToast(messageId: Int) {
        activity?.let {
            showToast(it.getString(messageId))
        }
    }

    private fun showToast(message: String) {
        activity?.let {
            Toast.makeText(it.applicationContext, message, Toast.LENGTH_LONG).show()
        }
    }

    private fun createTopLevelMediaSource(uriList: List<Uri>): MediaSource? {
        for (uri in uriList) {
            if (Util.maybeRequestReadExternalStoragePermission( /* activity= */
                    requireActivity(),
                    uri
                )
            ) {
                // The player will be reinitialized if the permission is granted.
                return null
            }
        }
        val mediaSources = arrayOfNulls<MediaSource>(uriList.size)
        for (i in uriList.indices) {
            mediaSources[i] = createLeafMediaSource(uriList[i])
        }
        return if (mediaSources.size == 1) mediaSources[0] else ConcatenatingMediaSource(*mediaSources)
    }

    private fun createLeafMediaSource(uri: Uri): MediaSource {
        val drmSessionManager: DrmSessionManager<ExoMediaCrypto> =
            DrmSessionManager.getDummyDrmSessionManager()
        return createLeafMediaSource(uri, drmSessionManager)
    }

    private fun createLeafMediaSource(
        uri: Uri,
        drmSessionManager: DrmSessionManager<*>
    ): MediaSource {
        @C.ContentType val type =
            Util.inferContentType(uri)
        return when (type) {
            C.TYPE_DASH -> DashMediaSource.Factory(mDataSourceFactory)
                .setDrmSessionManager(drmSessionManager)
                .createMediaSource(uri)
            C.TYPE_SS -> SsMediaSource.Factory(mDataSourceFactory)
                .setDrmSessionManager(drmSessionManager)
                .createMediaSource(uri)
            C.TYPE_HLS -> HlsMediaSource.Factory(mDataSourceFactory)
                .setDrmSessionManager(drmSessionManager)
                .createMediaSource(uri)
            C.TYPE_OTHER -> ProgressiveMediaSource.Factory(mDataSourceFactory)
                .setDrmSessionManager(drmSessionManager)
                .createMediaSource(uri)
            else -> throw IllegalStateException("Unsupported type: $type")
        }
    }

    private fun buildDataSourceFactory(): DataSource.Factory {
        val applicationContext = requireContext().applicationContext
        return buildDataSourceFactory(applicationContext, mUserAgent)
    }

    private fun buildRenderersFactory(preferExtensionRenderer: Boolean): RenderersFactory {
        @ExtensionRendererMode val extensionRendererMode =
            if (useExtensionRenderers()) if (preferExtensionRenderer) DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER else DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON else DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF
        return DefaultRenderersFactory( /* context= */requireContext())
            .setExtensionRendererMode(extensionRendererMode)
    }

    /** Returns whether extension renderers should be used.  */
    private fun useExtensionRenderers(): Boolean {
        return "withExtensions" == BuildConfig.FLAVOR
    }

    internal class PlayerEventListener : Player.EventListener {
        override fun onPlayerStateChanged(
            playWhenReady: Boolean,
            @Player.State playbackState: Int
        ) {
            val stateString: String = when (playbackState) {
                ExoPlayer.STATE_IDLE -> "ExoPlayer.STATE_IDLE      -"
                ExoPlayer.STATE_BUFFERING -> "ExoPlayer.STATE_BUFFERING -"
                ExoPlayer.STATE_READY -> "ExoPlayer.STATE_READY     -"
                ExoPlayer.STATE_ENDED -> "ExoPlayer.STATE_ENDED     -"
                else -> "UNKNOWN_STATE             -"
            }
        }

        override fun onPlayerError(e: ExoPlaybackException) {
        }

        override fun onTracksChanged(
            trackGroups: TrackGroupArray,
            trackSelections: TrackSelectionArray
        ) {
            for (i in 0 until trackGroups.length) {
                val trackGroup: TrackGroup = trackGroups[i]
                for (j in 0 until trackGroup.length) {
                    val trackMetadata: Metadata? =
                        trackGroup.getFormat(j).metadata
                    if (trackMetadata != null) {
                        Log.d("METADATA TRACK", trackMetadata.toString())
                    }
                }
            }
        }
    }

    override fun preparePlayback() {
        mPlayer?.retry()
    }

    private fun releasePlayer() {
        mPlayer?.let { player ->
            updateTrackSelectorParameters()
            updateStartPosition()
            val listener = mPlaybackStateListener as? Player.EventListener
            listener?.let {
                player.removeListener(it)
            }
            player.release()
            mPlayer = null
            mediaSource = null
            trackSelector = null
        }
    }

    private fun updateTrackSelectorParameters() {
        if (trackSelector != null) {
            trackSelectorParameters =
                trackSelector?.parameters ?: buildDefaultTrackSelectorParameters()
        }
    }

    private fun updateStartPosition() {
        mPlayer?.let { player ->
            startAutoPlay = player.playWhenReady
            startWindow = player.currentWindowIndex
            startPosition = Math.max(0, player.contentPosition).toLong()
        }
    }
}