package com.angdim.sedemosmi.tv.utils

import com.angdim.sedemosmi.tv.BuildConfig
import com.angdim.sedemosmi.tv.Constants

object Utils {
    private fun isExoPlayerEnabled(): Boolean {
        return BuildConfig.EXO_PLAYER_ENABLED
    }

    internal fun getVideoPlayerType(): String {
        return when (isExoPlayerEnabled()) {
            true -> Constants.VIDEO_FRAGMENT_EXO_PLAYER
            false -> Constants.VIDEO_FRAGMENT
        }
    }
}