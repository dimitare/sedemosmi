package com.angdim.sedemosmi.tv.utils

import android.app.Activity
import android.os.Build
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.angdim.sedemosmi.tv.R
import com.angdim.sedemosmi.tv.ui.common.ScrollChildSwipeRefreshLayout
import com.angdim.sedemosmi.tv.ui.common.fragment.VideoFragmentExoPlayer

fun Fragment.setupRefreshLayout(
    refreshLayout: ScrollChildSwipeRefreshLayout,
    scrollUpChild: View? = null
) {
    refreshLayout.setColorSchemeColors(
        ContextCompat.getColor(requireActivity(), R.color.colorPrimary),
        ContextCompat.getColor(requireActivity(), R.color.colorAccent),
        ContextCompat.getColor(requireActivity(), R.color.colorPrimaryDark)
    )
    // Set the scrolling view in the custom SwipeRefreshLayout.
    scrollUpChild?.let {
        //refreshLayout.scrollUpChild = it
    }
}

internal fun Fragment.hideSystemNavigation(activity: Activity?) {
    if (Build.VERSION.SDK_INT in Build.VERSION_CODES.JELLY_BEAN..Build.VERSION_CODES.JELLY_BEAN_MR2) { // lower api
        val v: View? = activity?.window?.decorView
        v?.systemUiVisibility = View.GONE
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        val decorView: View? =
            activity?.window?.decorView
        val uiOptions =
            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
        decorView?.systemUiVisibility = uiOptions;
    }
    val ab = (activity as AppCompatActivity?)?.supportActionBar
    ab?.hide()
}

internal fun Fragment.showSystemNavigation(activity: Activity?) {
    if (Build.VERSION.SDK_INT in Build.VERSION_CODES.JELLY_BEAN..Build.VERSION_CODES.JELLY_BEAN_MR2) { // lower api
        val v: View? = activity?.window?.decorView
        v?.systemUiVisibility = View.VISIBLE
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        val decorView: View? =
            activity?.window?.decorView
        val uiOptions =
            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        decorView?.systemUiVisibility = uiOptions;
    }
    val ab = (activity as AppCompatActivity?)?.supportActionBar
    ab?.show()
}

fun <T> Fragment.findFragmentByTag(tag: String): T? {
    val videoFragment = childFragmentManager.findFragmentByTag(tag)
    return videoFragment as T?
}