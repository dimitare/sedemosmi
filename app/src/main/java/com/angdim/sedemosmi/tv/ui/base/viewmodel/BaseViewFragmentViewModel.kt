package com.angdim.sedemosmi.tv.ui.base.viewmodel

import com.angdim.sedemosmi.tv.ui.base.viewmodel.BaseViewModel

abstract class BaseViewFragmentViewModel: BaseViewModel(){
    fun start() {
        startViewModel()
    }

    fun stop() {
        stopViewModel()
    }

    internal open fun startViewModel() {
    }

    internal open fun stopViewModel() {
    }
}