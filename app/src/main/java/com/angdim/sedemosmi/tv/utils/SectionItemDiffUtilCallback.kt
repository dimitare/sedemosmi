package com.angdim.sedemosmi.tv.utils

import androidx.recyclerview.widget.DiffUtil
import com.angdim.sedemosmi.tv.data.model.PageViews

class SectionItemDiffUtilCallback(
    newList: List<PageViews>,
    oldList: List<PageViews>
) :
    DiffUtil.Callback() {
    private val oldItems: List<PageViews>
    private val newItems: List<PageViews>
    override fun getOldListSize(): Int {
        return oldItems.size
    }

    override fun getNewListSize(): Int {
        return newItems.size
    }

    override fun areItemsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Boolean {
        return oldItems[oldItemPosition].videoUrlLink == newItems[newItemPosition].videoUrlLink
    }

    override fun areContentsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Boolean {
        return oldItems[oldItemPosition].videoUrlLink == newItems[newItemPosition].videoUrlLink
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        return super.getChangePayload(oldItemPosition, newItemPosition)
    }

    init {
        newItems = newList
        oldItems = oldList
    }
}