package com.angdim.sedemosmi.tv.ui

import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.angdim.sedemosmi.tv.Constants
import com.angdim.sedemosmi.tv.R
import com.angdim.sedemosmi.tv.ui.activities.BaseActivity
import com.angdim.sedemosmi.tv.ui.activities.main.MainActivityViewModel
import com.angdim.sedemosmi.tv.ui.base.fragment.VideoOrientation
import com.angdim.sedemosmi.tv.ui.main.home.HomeFragment
import com.angdim.sedemosmi.tv.ui.main.home.HomeFragmentDirections
import com.google.android.material.navigation.NavigationView

class MainActivity : BaseActivity<MainActivityViewModel>() {
    override val viewModel by viewModels<MainActivityViewModel>()

    private lateinit var mDrawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main)
        setupNavigationDrawer()

        val navController: NavController = findNavController(R.id.nav_host_fragment)
        findViewById<NavigationView>(R.id.nav_view)
            .setupWithNavController(navController)
    }

    private fun setupNavigationDrawer() {
        mDrawerLayout = (findViewById<DrawerLayout>(R.id.drawer_layout))
            .apply {
                setStatusBarBackground(R.color.colorPrimaryDark)
            }
        mDrawerLayout.addDrawerListener(object: DrawerLayout.DrawerListener{
            override fun onDrawerStateChanged(newState: Int) {

            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
            }

            override fun onDrawerClosed(drawerView: View) {
                supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_left_drawer_white)
            }

            override fun onDrawerOpened(drawerView: View) {
                supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_left_drawer_close_white)
            }

        })
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        val liveItem = navigationView.menu.findItem(R.id.live_fragment_dest)
        liveItem.setOnMenuItemClickListener {
            val action =
                HomeFragmentDirections.actionHomeFragmentToLiveFragment(provideLiveVideoUrl())
            findNavController(R.id.nav_host_fragment).navigate(action)
            mDrawerLayout.closeDrawer(GravityCompat.START)
            true
        }
    }

    private fun provideLiveVideoUrl(): String {
        val navigationFragment =
            supportFragmentManager.primaryNavigationFragment?.getTopFragment() as? HomeFragment
        navigationFragment?.let {
            return it.mViewModel.liveVideoUrl.value ?: Constants.LIVE_VIDEO_URL
        }
        return Constants.LIVE_VIDEO_URL
    }

    override fun onConfigurationChanged(configuration: Configuration) {
        super.onConfigurationChanged(configuration)
        if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            actionBar?.hide();
            window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            actionBar?.show();
        }
    }

    fun openCloseLeftDrawer() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START)
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START)
        }
    }
}

@Suppress("UNCHECKED_CAST")
fun Fragment.getTopFragment(): Fragment? {
    childFragmentManager.fragments.forEach {
        return it
    }
    return null
}