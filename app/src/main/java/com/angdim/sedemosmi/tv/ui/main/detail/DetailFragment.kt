package com.angdim.sedemosmi.tv.ui.main.detail

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.angdim.sedemosmi.tv.databinding.DetailLayoutBinding
import com.angdim.sedemosmi.tv.ui.common.fragment.VideoFragmentExoPlayer
import com.angdim.sedemosmi.tv.ui.common.fragment.VideoOrientationFragment
import com.angdim.sedemosmi.tv.utils.Utils
import com.angdim.sedemosmi.tv.utils.findFragmentByTag

class DetailFragment : VideoOrientationFragment<DetailFragmentViewModel>() {
    override val mViewModel by viewModels<DetailFragmentViewModel>()
    private lateinit var mDataBinding: DetailLayoutBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mDataBinding = DetailLayoutBinding.inflate(inflater, container, false).apply {
            viewModel = mViewModel
        }
        setUpActionBar()
        return mDataBinding.root
    }

    private fun setUpActionBar() {
        val ab = (activity as AppCompatActivity?)?.supportActionBar
        ab?.setDisplayHomeAsUpEnabled(false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: DetailFragmentArgs by navArgs()
        args.videoUrlLink?.let {
            mDataBinding.viewModel?.fetchVideoByUrl(it)
        }
        mDataBinding.viewModel?.videoUrl?.observe(viewLifecycleOwner, Observer {
            val videoFragment: VideoFragmentExoPlayer? = findFragmentByTag<VideoFragmentExoPlayer>(
                Utils.getVideoPlayerType()
            )
            videoFragment?.apply {
                if (it != null) {
                    setVideoUri(listOf(Uri.parse(it)))
                }
            }
        })
        mDataBinding.viewModel?.videoTitle?.observe(viewLifecycleOwner, Observer {
            val videoFragment: VideoFragmentExoPlayer? = findFragmentByTag<VideoFragmentExoPlayer>(Utils.getVideoPlayerType())
            videoFragment?.apply {
                setVideoTitle(it)
            }
        })
    }
}