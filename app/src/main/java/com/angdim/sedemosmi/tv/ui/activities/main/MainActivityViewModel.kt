package com.angdim.sedemosmi.tv.ui.activities.main

import com.angdim.sedemosmi.tv.ui.activities.BaseViewActivityViewModel
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(): BaseViewActivityViewModel() {}