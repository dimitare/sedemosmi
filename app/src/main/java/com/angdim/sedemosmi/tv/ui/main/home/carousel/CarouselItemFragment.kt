package com.angdim.sedemosmi.tv.ui.main.home.carousel

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.angdim.sedemosmi.tv.databinding.CarouselItemLayoutBinding
import com.angdim.sedemosmi.tv.ui.base.fragment.BaseDaggerFragment
import com.angdim.sedemosmi.tv.data.model.PageViews
import com.angdim.sedemosmi.tv.ui.main.home.HomeFragmentDirections

class CarouselItemFragment: BaseDaggerFragment<CarouselFragmentViewModel>(){
    override val mViewModel by viewModels<CarouselFragmentViewModel>()
    lateinit var binding : CarouselItemLayoutBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = CarouselItemLayoutBinding.inflate(
            LayoutInflater.from(inflater.context), container, false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pageView = arguments?.getParcelable<PageViews>("pageView")
        pageView?.let {
            binding.pageview = pageView
        }
        view.setOnClickListener {
            Log.w("", "pageView: $pageView")
            pageView?.videoUrlLink?.let {
                val direction = HomeFragmentDirections.actionHomeFragmentToDetailFragment(it)
                findNavController().navigate(direction)
            }
        }
    }
    companion object {
        @JvmStatic
        fun newInstance(pageView: PageViews) = CarouselItemFragment()
            .apply {
            arguments = Bundle().apply {
                putParcelable("pageView", pageView)
            }
        }
    }
}