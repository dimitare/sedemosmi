package com.angdim.sedemosmi.tv.data.model

import android.view.View

enum class VisibleStatus(val status: Int){
    VISIBLE(View.VISIBLE),
    INVISIBLE(View.INVISIBLE),
    GONE(View.GONE)
}