package com.angdim.sedemosmi.tv.ui.main.home

import androidx.lifecycle.*
import com.angdim.sedemosmi.tv.Constants
import com.angdim.sedemosmi.tv.Event
import com.angdim.sedemosmi.tv.di.scope.FragmentScope
import com.angdim.sedemosmi.tv.domain.GetHomepageUseCase
import com.angdim.sedemosmi.tv.ui.common.VisibleStatus
import com.angdim.sedemosmi.tv.ui.base.viewmodel.BaseViewFragmentViewModel
import com.angdim.sedemosmi.tv.data.model.Section
import com.angdim.sedemosmi.tv.data.services.remote.Status
import kotlinx.coroutines.launch
import javax.inject.Inject

@FragmentScope
class HomeFragmentViewModel @Inject constructor(val getHomepageUseCase: GetHomepageUseCase) :
    BaseViewFragmentViewModel() {
    private val _openTaskEvent = MutableLiveData<Event<String>>()
    val openLiveEvent: LiveData<Event<String>> = _openTaskEvent
    private val _homepage = MutableLiveData<List<Section>>()

    private val _dataLoading = MutableLiveData<Boolean>()

    private val _loading = MutableLiveData<VisibleStatus>()
    val dataLoading: LiveData<Boolean> = _dataLoading

    private val _liveVideoUrl = MutableLiveData<String>()
    val liveVideoUrl: LiveData<String> = _liveVideoUrl

    val mLoading = _loading.map {
        it
    }

    val homepage = _homepage

    override fun startViewModel() {
        super.startViewModel()
        if (_homepage.value == null) {
            fetchHomepage()
        }
    }

    private fun fetchHomepage() {
        _dataLoading.postValue(true)
        viewModelScope.launch {
            getHomepageUseCase().let {result ->
                when (result) {
                    is Status.Success -> {
                        _loading.postValue(VisibleStatus.GONE)
                        _dataLoading.postValue(false)
                        _homepage.postValue(result.data.sections)
                        _liveVideoUrl.postValue(result.data.liveVideo?.videoUrl ?: Constants.LIVE_VIDEO_URL)
                    }
                    is Status.Error -> {
                        _loading.postValue(VisibleStatus.GONE)
                        _dataLoading.postValue(false)
                        _liveVideoUrl.postValue(Constants.LIVE_VIDEO_URL)
                    }
                    Status.Loading -> {
                        _loading.postValue(VisibleStatus.VISIBLE)
                        _dataLoading.postValue(true)
                    }
                }
            }
        }
    }

    fun refresh() {
        _dataLoading.value = true
        viewModelScope.launch {
            fetchHomepage()
            _dataLoading.value = false
        }
    }
}