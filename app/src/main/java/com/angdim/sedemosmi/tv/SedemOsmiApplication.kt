package com.angdim.sedemosmi.tv

import android.content.Context
import androidx.multidex.MultiDex
import com.angdim.sedemosmi.tv.di.DaggerAppComponent
import dagger.android.DaggerApplication

class SedemOsmiApplication: DaggerApplication(){
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(newBase)
        MultiDex.install(this)
    }

    private val applicationInjector = DaggerAppComponent.builder()
        .application(this)
        .build()

    override fun applicationInjector() = applicationInjector
}