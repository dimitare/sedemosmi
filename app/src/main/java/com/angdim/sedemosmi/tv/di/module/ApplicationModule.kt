package com.angdim.sedemosmi.tv.di.module

import android.app.Application
import android.content.Context
import com.angdim.sedemosmi.tv.RetrofitModule
import com.angdim.sedemosmi.tv.SedemOsmiApplication
import com.angdim.sedemosmi.tv.SedemOsmiRemoteRepositoryModule
import com.angdim.sedemosmi.tv.ui.activities.ActivityBuilderModule
import com.angdim.sedemosmi.tv.di.qualifier.ApplicationContext
import dagger.Binds
import dagger.Module
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Module(
    includes = [AndroidSupportInjectionModule::class, ActivityBuilderModule::class, RetrofitModule::class, SedemOsmiRemoteRepositoryModule::class]
)
abstract class ApplicationModule {
    @Binds
    @Singleton
    abstract fun bindApplication(application: SedemOsmiApplication): Application

    @Binds
    @Singleton
    @ApplicationContext
    abstract fun bindApplicationContext(application: Application): Context
}