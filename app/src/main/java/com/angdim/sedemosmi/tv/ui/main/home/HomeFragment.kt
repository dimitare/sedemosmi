package com.angdim.sedemosmi.tv.ui.main.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.angdim.sedemosmi.tv.Constants
import com.angdim.sedemosmi.tv.EventObserver
import com.angdim.sedemosmi.tv.R
import com.angdim.sedemosmi.tv.databinding.HomeFragmentBinding
import com.angdim.sedemosmi.tv.ui.MainActivity
import com.angdim.sedemosmi.tv.ui.base.fragment.BaseDaggerFragment
import com.angdim.sedemosmi.tv.utils.setupRefreshLayout

class HomeFragment: BaseDaggerFragment<HomeFragmentViewModel>(){
    override val mViewModel by viewModels<HomeFragmentViewModel>()

    private lateinit var mDataBinding: HomeFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mDataBinding = HomeFragmentBinding.inflate(inflater, container, false).apply {
            viewmodel = mViewModel
        }
        mDataBinding.homepageContent.apply {
            adapter = HomepageAdapter(this@HomeFragment)
        }
        setUpActionBar()
        setupNavigation()
        setUpLiveVideoBtn()
        mDataBinding.lifecycleOwner = this
        return mDataBinding.root
    }

    private fun setUpActionBar() {
        val ab = (activity as AppCompatActivity?)?.supportActionBar
        ab?.setDisplayHomeAsUpEnabled(true)
        ab?.setHomeAsUpIndicator(R.drawable.ic_left_drawer_white)
    }

    private fun setupNavigation() {
        mViewModel.openLiveEvent.observe(viewLifecycleOwner, EventObserver {
            openLiveVideos(it)
        })
    }

    override fun onStart() {
        super.onStart()
        mDataBinding.viewmodel?.start()
    }

    override fun onStop() {
        super.onStop()
        mDataBinding.viewmodel?.stop()
    }

    private fun setUpLiveVideoBtn() {
        mDataBinding.homepageContent.setOnClickListener {
            launchLiveVideo()
        }
    }

    fun launchLiveVideo() {
        val live = mDataBinding.viewmodel?.liveVideoUrl?.value ?: Constants.LIVE_VIDEO_URL
        val action =
            HomeFragmentDirections.actionHomeFragmentToLiveFragment(live)
        findNavController().navigate(action)
    }

    private fun openLiveVideos(videoId: String) {
        val action = HomeFragmentDirections.actionHomeFragmentToLiveFragment(videoId)
        findNavController().navigate(action)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mDataBinding.viewmodel = mViewModel
        mDataBinding.lifecycleOwner = this.viewLifecycleOwner
        this.setupRefreshLayout(mDataBinding.refreshLayout)
    }

    override fun onDestroyView() {
        removeCarousel()
        super.onDestroyView()
    }

    private fun removeCarousel(){
        activity?.supportFragmentManager?.apply {
            findFragmentByTag(Constants.CAROUSEL_FRAGMENT)?.let {carousel ->
                val trans = beginTransaction()
                trans.remove(carousel)
                trans.commit()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val mainActivity = activity as? MainActivity
        mainActivity?.openCloseLeftDrawer()
        return super.onOptionsItemSelected(item)
    }
}