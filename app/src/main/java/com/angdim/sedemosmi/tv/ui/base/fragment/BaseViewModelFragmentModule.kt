package com.angdim.sedemosmi.tv.ui.base.fragment

import androidx.lifecycle.ViewModelProvider
import com.angdim.sedemosmi.tv.di.qualifier.FragmentContext
import com.angdim.sedemosmi.tv.di.scope.FragmentScope
import com.angdim.sedemosmi.tv.ui.common.viewmodel.FragmentViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class BaseViewModelFragmentModule {
    @Binds
    @FragmentScope
    @FragmentContext
    abstract fun bindViewModelFactory(viewModelFactory: FragmentViewModelFactory): ViewModelProvider.Factory
}