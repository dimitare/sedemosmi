package com.angdim.sedemosmi.tv.ui.activities

import android.app.Application
import android.content.Context
import com.angdim.sedemosmi.tv.ui.MainActivity
import com.angdim.sedemosmi.tv.ui.activities.main.MainActivityModule
import com.angdim.sedemosmi.tv.di.scope.ActivityScope
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import javax.inject.Singleton

@Module
abstract class ActivityBuilderModule {
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    @ActivityScope
    abstract fun contributeMainActivity(): MainActivity

    @Binds
    @Singleton
    abstract fun bindApplicationContext(application: Application): Context
}