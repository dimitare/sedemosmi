package com.angdim.sedemosmi.tv.ui.activities

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.angdim.sedemosmi.tv.di.qualifier.ActivityContext
import com.angdim.sedemosmi.tv.di.scope.ActivityScope
import dagger.Binds
import dagger.Module

@Module
abstract class BaseActivityModule {
    @Binds
    @ActivityScope
    @ActivityContext
    abstract fun bindActivityContext(activity: AppCompatActivity): Context

    @Binds
    @ActivityScope
    @ActivityContext
    abstract fun bindViewModelFactory(viewModelFactory: ActivityViewModelFactory): ViewModelProvider.Factory
}