package com.angdim.sedemosmi.tv.ui.activities

import androidx.lifecycle.ViewModel
import com.angdim.sedemosmi.tv.di.scope.ActivityScope
import com.angdim.sedemosmi.tv.ui.base.viewmodel.factory.BaseViewModelFactory
import javax.inject.Inject
import javax.inject.Provider

@ActivityScope
class ActivityViewModelFactory @Inject constructor(
    creators: Map<Class<out BaseViewActivityViewModel>,
            @JvmSuppressWildcards Provider<BaseViewActivityViewModel>>
) : BaseViewModelFactory(creators as Map<Class<out ViewModel>, Provider<ViewModel>>)