package com.angdim.sedemosmi.tv.data.model

import com.angdim.sedemosmi.tv.utils.HomepageDeserializer
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize

@JsonIgnoreProperties(ignoreUnknown = true)
data class HomepageResponse @JsonCreator constructor(
    @JsonProperty("code") val code : Int,
    @JsonDeserialize(using = HomepageDeserializer::class)
    @JsonProperty("sections") val sections : List<Section>,
    @JsonProperty("liveVideo") var liveVideo : LiveVideo? = null
)