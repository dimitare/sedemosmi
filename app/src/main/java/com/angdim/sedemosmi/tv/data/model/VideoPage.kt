package com.angdim.sedemosmi.tv.data.model

import com.angdim.sedemosmi.tv.utils.HomepageDeserializer
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize

@JsonIgnoreProperties(ignoreUnknown = true)
data class VideoPage @JsonCreator constructor(
    @JsonProperty("videoUrl") val videoUrl: String? = null,
    @JsonProperty("title") val title: String? = null,
    @JsonProperty("description") val description: String? = null,
    @JsonDeserialize(using = HomepageDeserializer::class)
    @JsonProperty("sections") val sections : List<Section>? = null
)