package com.angdim.sedemosmi.tv.ui.main.detail

import android.os.Build
import android.text.Html
import android.text.Spanned
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.angdim.sedemosmi.tv.data.model.PageViews
import com.angdim.sedemosmi.tv.data.model.Section
import com.angdim.sedemosmi.tv.data.services.remote.Status
import com.angdim.sedemosmi.tv.di.scope.FragmentScope
import com.angdim.sedemosmi.tv.domain.GetDetailPageUseCase
import com.angdim.sedemosmi.tv.ui.base.viewmodel.BaseViewFragmentViewModel
import com.angdim.sedemosmi.tv.ui.common.VisibleStatus
import kotlinx.coroutines.launch
import javax.inject.Inject

@FragmentScope
open class DetailFragmentViewModel @Inject constructor(open val getDetailPageUseCase: GetDetailPageUseCase) :
    BaseViewFragmentViewModel() {

    private val _videoUrl = MutableLiveData<String>()
    val videoUrl: LiveData<String> = _videoUrl

    private val _videoTitle = MutableLiveData<String>()
    val videoTitle: LiveData<String> = _videoTitle

    private val _description = MutableLiveData<String>()
    val description: LiveData<String> = _description

    private val _section = MutableLiveData<List<PageViews>>()
    val section: LiveData<List<PageViews>> = _section

    private val _errorVisibility = MutableLiveData<VisibleStatus>()
    val errorVisibility = MutableLiveData<VisibleStatus>()

    fun fetchVideoByUrl(videoUrlLink: String) {
        updateVideoUrl(null)
        updateErrorVisibility(VisibleStatus.GONE)
        viewModelScope.launch {
            getDetailPageUseCase(videoUrlLink).let { result ->
                when (result) {
                    is Status.Success -> {
                        updateVideoUrl(result.data.videoPage?.videoUrl)
                        updateVideoTitle(result.data.videoPage?.title)
                        updateDescription(result.data.videoPage?.description)
                        val sections = result.data.videoPage?.sections
                        sections?.let {
                            if(it.isNotEmpty()){
                                _section.postValue(it[0].pageViews)
                            }
                        }
                        updateErrorVisibility(VisibleStatus.GONE)
                    }
                    is Status.Error -> {
                        updateVideoUrl(null)
                        updateVideoTitle(null)
                        updateDescription(null)
                        updateErrorVisibility(VisibleStatus.VISIBLE)
                    }
                    Status.Loading -> {
                        updateVideoUrl(null)
                        updateVideoTitle(null)
                        updateDescription(null)
                        updateErrorVisibility(VisibleStatus.GONE)
                    }
                }
            }
        }
    }

    private fun updateVideoUrl(value: String?) {
        _videoUrl.postValue(value)
    }

    private fun updateVideoTitle(value: String?) {
        _videoTitle.postValue(value)
    }

    private fun updateDescription(value: String?) {
        _description.postValue(value)
    }

    private fun updateErrorVisibility(value: VisibleStatus) {
        _errorVisibility.postValue(value)
    }

    fun formantDescriptionText(description: String?): Spanned? {
        description?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT)
            } else {
                return Html.fromHtml(description)
            }
        } ?: return null
    }
}