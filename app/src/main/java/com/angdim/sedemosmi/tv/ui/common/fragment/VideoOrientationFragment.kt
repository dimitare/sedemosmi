package com.angdim.sedemosmi.tv.ui.common.fragment

import android.content.res.Configuration
import com.angdim.sedemosmi.tv.R
import com.angdim.sedemosmi.tv.ui.base.fragment.BaseDaggerFragment
import com.angdim.sedemosmi.tv.ui.base.fragment.VideoOrientation
import com.angdim.sedemosmi.tv.ui.base.viewmodel.BaseViewFragmentViewModel
import com.angdim.sedemosmi.tv.utils.hideSystemNavigation
import com.angdim.sedemosmi.tv.utils.showSystemNavigation

abstract class VideoOrientationFragment<VM : BaseViewFragmentViewModel>: BaseDaggerFragment<VM>(),
    VideoOrientation {
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        orientVideoDescriptionFragment(newConfig.orientation)
    }

    override fun orientVideoDescriptionFragment(orientation: Int) {
        // Hide the extra content when in landscape so the video is as large as possible.
        val fragmentManager = childFragmentManager
        val extraContentFragment = fragmentManager.findFragmentById(R.id.contentVideo)
        if (extraContentFragment != null) {
            val fragmentTransaction =
                fragmentManager.beginTransaction()
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                fragmentTransaction.hide(extraContentFragment)
                this.hideSystemNavigation(activity)
            } else {
                fragmentTransaction.show(extraContentFragment)
                this.showSystemNavigation(activity)
            }
            fragmentTransaction.commit()
        }
    }
}