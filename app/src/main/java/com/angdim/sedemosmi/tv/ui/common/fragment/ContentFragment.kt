package com.angdim.sedemosmi.tv.ui.common.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.angdim.sedemosmi.tv.databinding.FragmentContentVideoBinding
import com.angdim.sedemosmi.tv.ui.base.fragment.BaseDaggerFragment
import com.angdim.sedemosmi.tv.ui.main.detail.DetailFragmentDirections
import com.angdim.sedemosmi.tv.ui.main.detail.DetailFragmentViewModel
import com.angdim.sedemosmi.tv.ui.main.home.HomeFragmentDirections
import com.angdim.sedemosmi.tv.ui.main.home.section.SectionAdapter

open class ContentFragment: BaseDaggerFragment<DetailFragmentViewModel>() {
    override val mViewModel by viewModels<DetailFragmentViewModel>()
    lateinit var mDataBinding: FragmentContentVideoBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mDataBinding = FragmentContentVideoBinding.inflate(inflater, container, false).apply {
            viewModel = mViewModel
        }
        mDataBinding.lifecycleOwner = this
        return mDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDataBinding.content.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = SectionAdapter(listOf()) { pageView ->
                val direction =
                    DetailFragmentDirections.actionDetailFragmentToDetailFragment(pageView.videoUrlLink)
                findNavController().navigate(direction)
            }
        }
    }
}