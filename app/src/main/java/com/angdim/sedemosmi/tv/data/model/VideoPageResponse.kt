package com.angdim.sedemosmi.tv.data.model

import com.angdim.sedemosmi.tv.utils.HomepageDeserializer
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize

@JsonIgnoreProperties(ignoreUnknown = true)
data class VideoPageResponse @JsonCreator constructor(
    @JsonProperty("code") val code : Int,
    @JsonProperty("videoPage") val videoPage : VideoPage? = null
)