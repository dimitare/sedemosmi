package com.angdim.sedemosmi.tv.di.keys

import com.angdim.sedemosmi.tv.ui.activities.BaseViewActivityViewModel
import dagger.MapKey
import kotlin.reflect.KClass

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
@MustBeDocumented
annotation class ActivityViewModelKey(val value: KClass<out BaseViewActivityViewModel>)